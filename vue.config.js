module.exports = {
  devServer: {
    proxy: {
      '/' : {
        target: process.env.VUE_APP_TARGET,
        changeOrigin: true,
        ws: false
      }
    }
  },
};
