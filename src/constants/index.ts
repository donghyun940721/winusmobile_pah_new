/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	constants/index.ts
 *  Description:    Winus Mobile Delivery에 사용되는 상수 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.06. : Created by J. I. Cho
 *                  2023.09. : Refactoring by Y. S. Jung
 *
------------------------------------------------------------------------------*/
export interface CheckboxArrI {
  name: string;
  value: string;
}

export const checkboxArr = [
  {
    name: '양중없음',
    value: 'N',
  },
  {
    name: '양중',
    value: 'Y',
  },
  {
    name: '내림',
    value: 'D',
  },
  {
    name: '사다리차',
    value: 'H',
  },
  {
    name: '기타',
    value: 'E',
  },
];


export const PACKAGE_NAME = 'com.logisall.mobilewinusdelivery';

/** ROOT URL */
export const LOCAL_URL  = 'http://localhost:8090';
export const HOST_URL   = 'http://winus.poolathome.co.kr:8099';
export const PROXY_URL  = 'http://master.airovtech.com:8099/proxy';     // TODO :: 제거

/** 배송관리 */
export const LOGIN_QRY = 'IF_LOGIN_QRY.if';
export const ASP_SEND_MESSAGE_DATA = 'IF_FCM_PHONE_COMPLETE.if';
export const VERSION_QRY = 'IF_VERSION_QRY.if';
export const DELIVERY_DRIVER_LIST = 'IF_DELIVERY_DRIVER_LIST_QRY.if';
export const DELIVERY_SEARCH = 'IF_DELIVERY_DRIVER_HD_QRY.if';
export const ASP_ORDER_QRY = 'IF_DELIVERY_ORDER_QRY.if';
export const ASP_PHONE_QRY = 'IF_DELIVERY_PHONE_QRY.if';
export const ASP_ORDER_DETAIL_QRY = 'IF_DELIVERY_ORDER_DETAIL_QRY2.if';
export const ASP_SEARCH_QRY = 'IF_ITEM_INFO_QRY.if';
export const ASP_SEND_START_DATA = 'IF_DELIVERY_START_DATA.if';
export const ASP_SEND_MANAGE_CANCEL_DATA = 'IF_DELIVERY_CANCEL_DATA.if';
export const ASP_SEND_DATE_CHANGE_DATA = 'IF_DELIVERY_CHANGE_REQ_DATE.if';
export const ASP_SEND_MANAGE_DATA = 'IF_DELIVERY_MANAGE_DATA.if';

// 출발후취소 / 취소요청 조회 쿼리 변경되어야함 : ASP_ORDER_QRY  → ASP_ORDER_DETAIL_QRY


/** AS 관리 */

// export const ASP_SEND_REQ_CANCEL_DATA = 'IF_DELIVERY_COMPLETE_20200614.if'; // 12장
export const ASP_SEND_REQ_CANCEL_DATA = 'IF_DELIVERY_COMPLETE_12RBF_20230717.ifs';

// export const ASP_SEND_REQ_ELEC_SERIAL = 'IF_DELIVERY_COMPLETE_20220501.if';
export const ASP_SEND_REQ_ELEC_SERIAL = 'IF_DELIVERY_COMPLETE_21RBF_20230717.ifs';  // 20장


export const AS_DELIVERY_DRIVER_LIST = 'IF_AS_DELIVERY_DRIVER_LIST_QRY.if'; 
export const AS_DELIVERY_SEARCH = 'IF_AS_DELIVERY_DRIVER_QRY.if';
export const AS_DELIVERY_ORDER = 'IF_AS_DELIVERY_ORDER_QRY.if';
export const AS_DELIVERY_MANAGE_DATA = 'IF_AS_DELIVERY_MANAGE_DATA.if';
export const AS_DELIVERY_ORDER_CHANGE = 'IF_AS_DELIVERY_ORDER_CHANGE_QRY.if';
export const AS_DELIVERY_CHANGE_REQ_DATE = 'IF_AS_DELIVERY_CHANGE_REQ_DATE.if';
export const AS_DELIVERY_SEND_MSG_DATA = 'IF_AS_DELIVERY_SEND_MSG_DATA.if';
export const AS_DELIVERY_START_DATA = 'IF_AS_DELIVERY_START_DATA.if';

export const AS_DELIVERY_SRH_MEMO = '/IF_DELIVERY_AS_SEARCH_QRY.if';  // 인수증메모 조회쿼리 


export const AS_REPAIR_DRIVER_LIST = 'IF_AS_REPAIR_DRIVER_QRY.if'; // as완료(as택배발송리스트) 조회
export const AS_REPAIR_DETAIL_LIST = 'IF_REPAIR_ORDER_DETAIL_QRY.if';  // 상세 조회 
export const AS_REPAIR_ORDER_QRY = 'IF_AS_REPAIR_ORDER_QRY.if';

//export const AS_ASP_SEND_REQ_CANCEL_DATA = 'IF_AS_DELIVERY_COMPLETE_20200614.if'; 
export const AS_ASP_SEND_REQ_CANCEL_DATA = 'IF_AS_DELIVERY_COMPLETE_12RBF_20230717.ifs';// SYS

export const AS_DELIVERY_NOT_SET_DRIVER = 'IF_DELIVERY_AS_NOT_SET_DRIVER_QRY.if';       // AS 배송기사 조회
export const DELIVERY_NOT_SET_DRIVER = 'IF_DELIVERY_NOT_SET_DRIVER_QRY.if';       // 배송기사 조회


/* WMS *///IF_MOVE_FROM_REFURBISHED_QRY

export const WMS_KIT_ITEM_LIST = 'IF_RITEM_SEARCH_QRY.if';
export const WMS_KIT_ITEM_DETAIL_LIST = 'IF_MOVE_FROM_REFURBISHED_QRY.if';

export const elecSerialCameraArr = [
  '인수증',
  '시리얼번호',
  '구성품',
  '설치사진1',
  '설치사진2',
  '설치사진3',
  '설치사진4',
  '설치사진5',
  '설치사진6',
];

export const reqCancelCameraArr = [
  '인수증',
  '해피콜여부',
  '집앞사진',
  '기타',
  '기타',
  '기타',
  '기타',
  '기타',
  '기타',
];

import { v4 } from 'uuid';

export interface ListItemI {
  id: string;
  category: string;
  list: SubListItem[];
}

interface SubListItem {
  id: string;
  title: string;
  path: string;
}

export const MENU_LIST = [
  {
    id: v4(),
    category: '배송',
    list: [
      { id: v4(), title: '배송관리', path: '/delivery' },
      { id: v4(), title: '배송예정일 지정', path: '/delivery/date-change-manage' },
    ],
  },
  {
    id: v4(),
    category: 'AS관리',
    list: [
      { id: v4(), title: 'AS관리', path: '/as/search' },
      { id: v4(), title: '배송예정일 지정', path: '/as/date-change-manage' },
      { id: v4(), title: 'AS택배발송', path: '/repair/search' },
    ],
  },
  {
    id: v4(),
    category: 'WMS',
    list: [
      { id: v4(), title: '작업관리', path: '/wms' }
    ],
  }
];

export const DELIVERY_LIST = [
  {
    id: v4(),
    category: '배송완료',
    list: [
      {
        id: v4(),
        title: '배송조회(해피콜/문자전송)',
        path: '/delivery/search',
      },
    ],
  },

  {
    id: v4(),
    category: '데이터 일괄 전송',
    list: [
      {
        id: v4(),
        title: '데이터 일괄 전송',
        path: '/delivery',
      },
    ],
  },
];

export const WMS_MENU_LIST = [
  {
    id: v4(),
    category: '임가공',
    list: [
      { id: v4(), title: '임가공', path: '/wms/kitComplete' }
    ],
  }
];


export const SMS_MESSAGE1 = '고객님 안녕하세요.';
export const SMS_MESSAGE2 = ' 배송 기사 입니다.';
export const SMS_MESSAGE3 = '고객님께서 주문하신 ';
export const SMS_MESSAGE4 = '상품명: ';
export const SMS_MESSAGE5 = '를, 배송주소: ';
export const SMS_MESSAGE6 = '로 배송 예정입니다.';
export const SMS_MESSAGE7 = ' 기사명: ';
export const SMS_MESSAGE8 = ', 전화번호: ';

export const PICTURE_QUALITY = 70;

export const SELECTED_TIME = [
  { value: '0600', time: '06:00 ~ 06:30' },
  { value: '0630', time: '06:30 ~ 07:00' },
  { value: '0700', time: '07:00 ~ 07:30' },
  { value: '0730', time: '07:30 ~ 08:00' },
  { value: '0800', time: '08:00 ~ 08:30' },
  { value: '0830', time: '08:30 ~ 09:00' },
  { value: '0900', time: '09:00 ~ 09:30' },
  { value: '0930', time: '09:30 ~ 10:00' },
  { value: '1000', time: '10:00 ~ 10:30' },
  { value: '1030', time: '10:30 ~ 11:00' },
  { value: '1100', time: '11:00 ~ 11:30' },
  { value: '1130', time: '11:30 ~ 12:00' },
  { value: '1200', time: '12:00 ~ 12:30' },
  { value: '1230', time: '12:30 ~ 13:00' },
  { value: '1300', time: '13:00 ~ 13:30' },
  { value: '1330', time: '13:30 ~ 14:00' },
  { value: '1400', time: '14:00 ~ 14:30' },
  { value: '1430', time: '14:30 ~ 15:00' },
  { value: '1500', time: '15:00 ~ 15:30' },
  { value: '1530', time: '15:30 ~ 16:00' },
  { value: '1600', time: '16:00 ~ 16:30' },
  { value: '1630', time: '16:30 ~ 17:00' },
  { value: '1700', time: '17:00 ~ 17:30' },
  { value: '1730', time: '17:30 ~ 18:00' },
  { value: '1800', time: '18:00 ~ 18:30' },
  { value: '1830', time: '18:30 ~ 19:00' },
  { value: '1900', time: '19:00 ~ 19:30' },
  { value: '1930', time: '19:30 ~ 20:00' },
  { value: '2000', time: '20:00 ~ 20:30' },
  { value: '2030', time: '20:30 ~ 21:00' },
  { value: '2100', time: '21:00 ~ 21:30' },
  { value: '2130', time: '21:30 ~ 22:00' },
  { value: '2200', time: '22:00 ~ 22:30' },
  { value: '2230', time: '22:30 ~ 23:00' },
  { value: '2300', time: '23:00 ~ 23:30' },
  { value: '2330', time: '23:30 ~ 24:00' },
];

export const WORK_STAT = {
  '09': '배송보류',
  '10': '배송대기',
  '20': '배송출발',
  '21': '출발 후 취소',
  '30': '현장취소',
  '80': '미배송',
  '87': '초도불량',
  '89': '취소요청',
  '90': '취소',
  '91': '펑크',
  '99': '배송완료',
};

export const AS_WORK_STAT = {
  '09': '배송보류',
  '10': '배송대기',
  '20': '배송출발',
  '21': '출발 후 취소',
  '30': '현장취소',
  '80': '미배송',
  '87': '초도불량',
  '89': '취소요청',
  '90': '취소',
  '91': '펑크',
  '99': '배송완료',
};


export const REPAIR_STAT = {
  'ASREP': 'ASREP',
  'ASNREP': 'ASNREP',
};