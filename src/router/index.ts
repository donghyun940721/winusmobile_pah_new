/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	router/index.ts
 *  Description:    Winus Mobile Delivery 페이지 라우딩 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.06. : Created by J. I. Cho
 *                  2023.09. : Refactoring by Y. S. Jung
 *
------------------------------------------------------------------------------*/
import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';
import LoginPage from '../views/LoginPage.vue';
import MenuPage from '../views/MenuPage.vue';
import WmsPage from '../views/WmsPage.vue';

import DeliveryPage from '../views/DeliveryPage.vue';
import DeliverySearch from '../views/delivery/DeliverySearch.vue';
import DeliveryManage from '../views/delivery/DeliveryManage.vue';
import CompleteSerial from '../views/delivery/DeliveryCompHdElecSerial.vue';
import DateChange from '../views/delivery/DeliveryReqDateChange.vue';
import DeliveryStart from '../views/delivery/DeliveryStart.vue';
import DeliveryCancel from '../views/delivery/DeliveryReqCancel.vue';
import DeliveryCancelAfter from '../views/delivery/DeliveryCompCancelAfterStart.vue';
import DetailSearch from '../views/delivery/DlvItemDetailInfoSearch.vue';
import ManageCancel from '../views/delivery/DeliveryCancelReq.vue';
import DeliveryReqDateManage from '../views/delivery/DeliveryReqDateManage.vue';


import AsSearch from '../views/as/AsSearch.vue';
import RepairSearch from '../views/as/RepairSearch.vue';
import AsReqDateManage from '../views/as/AsReqDateManage.vue';
import AsManage from '../views/as/AsManage.vue';
import AsDateChange from '../views/as/AsReqDateChange.vue';
import AsStart from '../views/as/AsStart.vue';
import SrhMemo from '../views/as/DeliverySrhMemo.vue';
import DlvSrhMemo from '../views/delivery/DeliverySrhMemo.vue';
import AsDetailSearch from '../views/as/DlvAsItemDetailInfoSearch.vue';
import AsCancel from '../views/as/AsReqCancel.vue';
import AsCompleteSerial from '../views/as/AsCompHdSerial.vue';
import RepairComplete from '../views/as/RepairComplete.vue';


import KitComplete from '../views/wms/KitComplete.vue';






const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/login',
  },
  {
    path: '/login',
    name: 'Login',
    component: LoginPage,
  },
  {
    path: '/menu',
    name: 'Menu',
    component: MenuPage,
  },
  {
    path: '/wms',
    name: 'Wms',
    component: WmsPage,
  },
  {
    path: '/delivery',
    name: 'Delivery',
    component: DeliveryPage,
  },
  {
    path: '/delivery/search',
    name: 'DeliverySearch',
    component: DeliverySearch,
  },
  {
    path: '/delivery/manage',
    name: 'DeliveryManage',
    component: DeliveryManage,
  },
  {
    path: '/delivery/complete-serial',
    name: 'CompleteSerial',
    component: CompleteSerial,
  },
  {
    path: '/delivery/date-change',
    name: 'DateChange',
    component: DateChange,
  },
  {
    path: '/delivery/start',
    name: 'DeliveryStart',
    component: DeliveryStart,
  },
  {
    path: '/delivery/cancel',
    name: 'DeliveryCancel',
    component: DeliveryCancel,
  },
  {
    path: '/delivery/cancel-after',
    name: 'DeliveryCancelAfter',
    component: DeliveryCancelAfter,
  },
  {
    path: '/delivery/detail-search',
    name: 'DetailSearch',
    component: DetailSearch,
  },
  {
    path: '/delivery/manage/cancel',
    name: 'ManageCancel',
    component: ManageCancel,
  },
  {
    path: '/delivery/date-change-manage',
    name: 'DeliveryReqDateManage',
    component: DeliveryReqDateManage,
  },
  {
    path: '/delivery/srh-memo',
    name: 'DlvSrhMemo',
    component: DlvSrhMemo,
  },
  // AS 관리
  {
    path: '/as/search',
    name: 'AsSearch',
    component: AsSearch,
  },
  {
    path: '/as/date-change-manage',
    name: 'AsReqDateManage',
    component: AsReqDateManage,
  },   
   {
    path: '/repair/search',
    name: 'RepairSearch',
    component: RepairSearch,
  },
  {
    path: '/as/manage',
    name: 'AsManage',
    component: AsManage,
  },
  {
    path: '/as/date-change',
    name: 'AsDateChange',
    component: AsDateChange,
  },
  {
    path: '/as/start',
    name: 'AsStart',
    component: AsStart,
  },
  {
    path: '/as/srh-memo',
    name: 'SrhMemo',
    component: SrhMemo,
  },
  {
    path: '/as/detail-search',
    name: 'AsDetailSearch',
    component: AsDetailSearch,
  },
  {
    path: '/as/as-cancel',
    name: 'AsCancel',
    component: AsCancel,
  },
  {
    path: '/as/complete-serial',
    name: 'AsCompleteSerial',
    component: AsCompleteSerial,
  },
  {
    path: '/repair/complete',
    name: 'RepairComplete',
    component: RepairComplete,
  },
  {
    path: '/wms/kitComplete',
    name: 'KitComplete',
    component: KitComplete,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
