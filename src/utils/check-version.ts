/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	check-version.ts
 *  Description:    APP Version 관리 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.06. : Created by J. I. Cho
 *                  2023.09. : Refactoring by Y. S. Jung
 *
------------------------------------------------------------------------------*/
import { VERSION_QRY } from '@/constants';
import { getApi } from './api-hook';
import axios from 'axios';

const PACKAGE_NAME = 'com.logisall.mobilewinus';
const user_id = localStorage.getItem('id') || '';

const isEmpty = (val: object) =>
  Object.keys(val).length === 0 && val.constructor === Object;

export const checkLastestVersion = async () => {
  try {
    const response = await getApi(VERSION_QRY, {
      login_id: user_id,
      password: '',
      ord_id: 'DERLIVERY_MOBILE',
    });

    const lastestVersion = response.data.DATA[0].program_id;
    const Sversion = sessionStorage.version.split('.');
    const Lversion = lastestVersion.split('.');
    const SversionVal = Sversion[2];
    const LversionVal = Lversion[2];

    if (!isEmpty(lastestVersion) && SversionVal < LversionVal) {
      if (confirm('신규버전이 존재합니다. 업데이트 하시겠습니까 ?')) {
        const exec = document.createElement('a');
        const link =
          'https://play.google.com/store/apps/details?id=' + PACKAGE_NAME;
        exec.setAttribute('href', link);
        exec.click();
      }
    }
  } catch (err) {
    console.log('error: ' + err);
  }
};
