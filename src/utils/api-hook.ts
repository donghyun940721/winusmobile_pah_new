/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	api-hook.ts
 *  Description:    Rest API를 연결해주는 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.06. : Created by J. I. Cho
 *                  2023.09. : Refactoring by Y. S. Jung
 *
------------------------------------------------------------------------------*/
import { loadingController } from '@ionic/vue';
import axios from 'axios';
import { LOCAL_URL, HOST_URL, PROXY_URL } from '@/constants';

import {
  CapacitorHttp,
  HttpHeaders
} from '@capacitor/core';


/*
 *
 * 1) Web 실행
 *  환경변수(.env.*)를 참조하는 proxy 설정을 서비스 타겟으로 함.
 *  package.json 스크립트 명령어 참고 ("npm run dev")
 *  
 *  API Service로 axios 사용.
 * 
 * 2) App 실행
 *  아래 API Hook에서 사용되는 각 HOST_URL, LOCAL_URL 설정하여 서비스 타겟 설정.
 *  
 *  API Service로 CapacitorHTTP 사용.
 * 
 */

export const getApi = async (url: string, params: any) => {
  const loading = await loadingController.create({});
  await loading.present();
  
  // 웹 - 개발
  if(process.env.NODE_ENV == "development"){
    const response = axios.get(url, {
      params: params,
    });
    await loading.dismiss();
    return response;
  }
  // 모바일 - 빌드
  else{
    const response = await CapacitorHttp.get({
      // url: LOCAL_URL + '/' + url,
      url: HOST_URL + '/' + url,
      // url: PROXY_URL + '/' + url,
      params,
      responseType: 'json',
    });  
    await loading.dismiss();
    return response;
  }
};

export const postApi = async (url: string, data: any, headers: HttpHeaders) => {
  const loading = await loadingController.create({});
  await loading.present();

  // 웹 - 개발
  if(process.env.NODE_ENV == "development"){
    const response = axios.post(url, {
      params: data,
    });
    await loading.dismiss();
    return response;
  } 
  // 모바일 - 빌드
  else {
    const response = await CapacitorHttp.post({
      // url: LOCAL_URL + '/' + url,
      url: HOST_URL + '/' + url,
      // url: PROXY_URL + '/' + url,
      data,
      headers,
      responseType: 'json',
    });
    await loading.dismiss();
    return response;
  }
};
