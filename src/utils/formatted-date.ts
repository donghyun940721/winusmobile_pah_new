/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	formatted-datㄷ.ts
 *  Description:    Date를 format해주는 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.06. : Created by J. I. Cho
 *                  2023.09. : Refactoring by Y. S. Jung
 *
------------------------------------------------------------------------------*/
export const formattedDate = (date: string) => {
  return `${date.substring(0, 4)}-${date.substring(4, 6)}-${date.substring(6)}`;
};
export const formattedFromDate = (date: Date, delimiter = '-') => {
  const year = date.getFullYear();
  const month = leftPad(date.getMonth() + 1);
  const day = leftPad(date.getDate());

  return [year, month, day].join(delimiter);
}
function leftPad(value: any) {
  if (value >= 10) {
      return value;
  }

  return `0${value}`;
}
