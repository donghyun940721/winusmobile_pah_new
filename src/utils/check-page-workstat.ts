/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	check-page-workstat.ts
 *  Description:    Check Page Work Stat 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.06. : Created by J. I. Cho
 *                  2023.09. : Refactoring by Y. S. Jung
 *
------------------------------------------------------------------------------*/
import { AS_WORK_STAT, WORK_STAT } from '@/constants';

export const checkPageWorkStat = () => {
  const isDeliveryPage = window.location.pathname.startsWith('/delivery');

  return isDeliveryPage ? WORK_STAT : AS_WORK_STAT;
};
