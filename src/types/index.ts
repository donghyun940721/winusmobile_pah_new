/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	types/index.ts
 *  Description:    Winus Mobile Delivery에 사용되는 데이터형식 및 인터페이스 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.06. : Created by J. I. Cho
 *                  2023.09. : Refactoring by Y. S. Jung
 *
------------------------------------------------------------------------------*/
export interface DetailI {
  addr: string;
  customer_nm: string;
  dlv_etc1: string;
  dlv_etc2: string;
  dlv_etc3: string;
  dlv_etc4: string;
  dlv_etc5: string;
  dlv_etc7?: string;
  dlv_ord_type: string;
  dlv_visit_cost: number;
  driver_nm: string;
  driver_phone: string;
  happy_call_yn: string;
  happy_call_memo: string;
  ord_id: string;
  phone1: string;
  phone2: string;
  product_nm?: string;
  product_nm2?: string;
  product_nm3?: string;
  product_nm4?: string;
  product_nm5?: string;
  product_nm6?: string;
  product_nm7?: string;
  product_nm8?: string;
  qty: number;
  req_dt: string;
  work_stat: string;
  dlv_time_from?: number;
  ord_seq?: string;
  
}

export interface SearchQryI {
  cust_id: string;
  cust_cd: string;
  cust_nm: string;
  item_bar_cd: string;
  item_grp_nm: string;
  ord_type: string;
  ord_wright: number;
  remark: string;
  ritem_cd: string;
  ritem_id: string;
  ritem_nm: string;
  uom_id: string;
  uom_nm: string;
  loc_cd?: string;
  prop_chk?: string;
  work_stat?: string;
}

export interface SerialListI {
  epc_cd: string;
  item_serial_list: {
    child_cust_lot_no?: string;
    map_item_qty: number;
    map_item_bar_cd: string;
  }[];
}

export interface SrhMemoListI {
  addr: string;
  customer_nm: string;
  dlv_comment: string;
  dlv_etc1: string;
  dlv_etc2: string;
  dlv_etc3: string;
  dlv_etc4: string;
  dlv_etc5: string;
  dlv_ord_type: string;
  dlv_pay_req_yn: string;
  driver_nm: string;
  driver_phone: string;
  happy_call_yn: string;
  ord_id: string;
  phone1: string;
  phone2: string;
  product_nm: string;
  qty: number;
  req_dt: string;
  work_stat: string;
  etc1?: string;
  happy_call_memo?: string;
}

export interface DriverListI {
  driver_nm: string;
  driver_phone: string;
}

export interface PictureI {
  id: string;
  title: string;
}

export interface SelectTypeI {
  value: string;
  title: string;
}



export interface ItemInfoListI {
  RITEM_CD: string;
  RITEM_NM: string;
  RITEM_ID: string;
  VIEW_NM: string;
}



export interface AsQryInfoListI {
  ADDR     : string;
  CUSTOMER_NM   : string;
  DLV_COMMENT   : string;
  DLV_ERROR_CD   : string;
  DLV_ERROR_NM   : string;
  DLV_ETC1   : string;
  DLV_ETC2   : string;
  DLV_ETC3   : string;
  DLV_ETC4   : string;
  DLV_ETC5   : string;
  DLV_IMG1   : string;
  DLV_ORD_TYPE   : string;
  DLV_PART_PRICE   : string;
  DLV_PAY_REQ_YN   : string;
  DLV_SETUP_DT   : string;
  DLV_TIME_FROM   : string;
  DLV_TIME_TO   : string;
  DLV_VISIT_COST   : string;
  DLV_WORK_COST   : string;
  DRIVER_NM   : string;
  DRIVER_PHONE   : string;
  ETC1: string;
  ETC2: string;
  ETC3: string;
  HAPPY_CALL_MEMO   : string;
  HAPPY_CALL_YN   : string;
  ORD_ID   : string;
  PHONE1   : string;
  PHONE2   : string;
  PRODUCT_NM   : string;
  QTY: string;
  REQ_DT   : string;
  WORK_STAT   : string;    
  
}


export interface ItemDetailListI {
  lc_id: string;           	
  user_no: string;         	
  lang: string;       		
  program_id: string;       
  ord_id: string;       	
  ord_seq: string;       	
  ord_qty: number;       	
  ord_type: string;       	
  ord_subtype: string;      
  ritem_id: string;       	
  ritem_cd: string;       	
  ritem_nm: string;       	
  cust_id: string;       	
  cust_type: string;       	
  cust_cd: string;       	
  cust_nm: string;       	
  trans_cust_cd: string;    
  ord_weight: string;       
  min_wgt: string;       	
  max_wgt: string;       	
  cust_lot_no_m: string;    
  loc_id: string;       	
  loc_cd: string;       	
  loc_barcode_cd: string;   
  uom_id: string;       	
  make_date: string;       	
  item_bar_cd: string;      
  epc_cd: string;       	
  rti_epc_cd: string;       
  before_stock_qty: string; 
  in_qty: string;       	
  out_qty: number;       	
  stock_qty: string;       	
  bad_qty: string;       	
  as_qty: string;       	
  item_grp_nm: string;      
  uom_nm: string;       	
  remark: string;       	
  event_qty: string;       	
  prop_qty: string;       	
  return_qty: string;       
  prop_chk: string;       	
  wh_id: string;       		
  cross_qty: string;       	
  work_stat: number;       	
  in_dt: string;       		
  out_dt: string;       	
  total_qty: string;       	
  remain_qty: string;   
}
